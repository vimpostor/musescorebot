WARNING: Unfortunately this bot is no longer working, because Musescore decided to make their API unavailable to the public again. Apparently it got misused by some developers and now all API users have to pay for this. Note that this bot only provided links to the Musescore website and never misused the API.
Unless Musescore changes their mind, there is no legal way to make this bot work again.

This is the source code for the MuseScore Telegram bot available at [@musescorebot](https://telegram.me/musescorebot) - licensed under the terms of the AGPLv3.

# Installation

First install `Go` and make sure that you have `telegram-bot-api` installed:

```bash
go get -u github.com/go-telegram-bot-api/telegram-bot-api
```

Then you can start the bot:

```bash
go run main.go -bottoken "Your Telegram bot token" -cert /path/to/public_cert.pem -iport port_to_listen_on -key /path/to/private.key -musescorekey "Your Musescore API key" -oport port_that_telegram_expects -url "The URL that Telegram expects"
```
Example:

```bash
go run main.go -bottoken "123456789:ABCDE-FGHIJKLMNOPQRSTUVWXYZABCDEFGH" -cert /home/ms/public_cert.pem -iport 8901 -key /home/ms/private.key -musescorekey "abcdefghijklmnopqrstuvwxyz012345" -oport 88 -url "https://example.com"
```

If you run the bot for the first time, you will need to register the webhook. You can do this either by adding the option `-setwebhook` or by using curl directly:

```bash
# set the webhook
curl -F "url=https://example.com:OUTSIDEPORT/<YOUR_BOT_TOKEN>" -F "certificate=/path/to/cert" 'https://api.telegram.org/bot<YOUR_BOT_TOKEN>/setWebHook'
# get the webhook info
curl 'https://api.telegram.org/bot<YOUR_BOT_TOKEN>/getWebhookInfo'
```

# Advanced

If you use different inner and outer ports, you will need to configure your firewall to forward the ports adequately, e.g. for iptables:

```bash
# redirect traffic from outer port 88 to inner port 8901
iptables -t nat -I OUTPUT -p tcp -d 127.0.0.1 --dport 88 -j REDIRECT --to-ports 8901
# make the change permanent, this requires the iptables service to be enabled
iptables-save > /etc/iptables/iptables.rules
```
