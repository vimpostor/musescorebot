package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"

	"github.com/go-telegram-bot-api/telegram-bot-api"
)

type score struct {
	Title     string `json:"title"`
	ViewCount string `json:"view_count"`
	PermaLink string `json:"permalink"`
	Id        string `json:"id"`
	Secret    string `json:"secret"`
}

func getScores(query string, msToken string) []tgbotapi.InlineQueryResultArticle {
	scores := []tgbotapi.InlineQueryResultArticle{}
	fileGenUrl := "http://musescore.com/static/musescore/scoredata/gen/%s/%s/%s/%s/%s/"
	if query == "" {
		// no need to query for the empty string
		return scores
	}
	addAdditionalFiles := false
	if strings.HasSuffix(query, "|pdf") {
		query = strings.TrimSuffix(query, "|pdf")
		addAdditionalFiles = true
	}
	url := fmt.Sprintf("http://api.musescore.com/services/rest/score.json?oauth_consumer_key=%s&text=%s", msToken, query)
	resp, err := http.Get(url)
	if err != nil {
		log.Println(err)
	} else {
		defer resp.Body.Close()
		var dat []score
		body, _ := ioutil.ReadAll(resp.Body)
		if err := json.Unmarshal(body, &dat); err != nil {
			log.Println(err)
		}
		for _, score := range dat {
			inlineQueryResultBody := score.PermaLink
			length := len(score.Id)
			idX := string(score.Id[length-1])
			idY := string(score.Id[length-2])
			idZ := string(score.Id[length-3])
			if addAdditionalFiles {
				inlineQueryResultBody += "\n" + fmt.Sprintf(fileGenUrl+"score.pdf", idX, idY, idZ, score.Id, score.Secret)
			}
			inlineResult := tgbotapi.NewInlineQueryResultArticle(query+score.Id, score.Title+" ["+score.ViewCount+"]", inlineQueryResultBody)
			inlineResult.ThumbURL = fmt.Sprintf(fileGenUrl+"thumb.png", idX, idY, idZ, score.Id, score.Secret)
			scores = append(scores, inlineResult)
		}
	}
	return scores
}

func main() {
	botToken := flag.String("bottoken", "", "The token of your Telegram bot")
	url := flag.String("url", "", "The URL that the bot listens on, e.g. https://example.com")
	insidePort := flag.Int("iport", 8443, "The port that the bot will listen on")
	outsidePort := flag.Int("oport", 8443, "The port that the Telegram api will send updates to")
	cert := flag.String("cert", "", "The location of your public cert.pem file")
	key := flag.String("key", "", "The location of your private key file")
	setWebhook := flag.Bool("setwebhook", false, "Setting this option will automatically update the webhook info on Telegram's side prior to starting the bot")
	msToken := flag.String("musescorekey", "", "Your Musescore API key")
	flag.Parse()
	bot, err := tgbotapi.NewBotAPI(*botToken)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Authorized on account %s", bot.Self.UserName)
	if *setWebhook {
		_, err = bot.SetWebhook(tgbotapi.NewWebhookWithCert(*url+":"+strconv.Itoa(*outsidePort)+"/"+bot.Token, *cert))
		if err != nil {
			log.Fatal(err)
		}
		info, err := bot.GetWebhookInfo()
		if err != nil {
			log.Fatal(err)
		}
		if info.LastErrorDate != 0 {
			log.Printf("Telegram callback failed: %s", info.LastErrorMessage)
		} else {
			log.Println("The webhook was updated successfully.")
		}
	}
	updates := bot.ListenForWebhook("/" + bot.Token)
	go http.ListenAndServeTLS("0.0.0.0:"+strconv.Itoa(*insidePort), *cert, *key, nil)

	for update := range updates {
		if update.Message != nil {
			msg := tgbotapi.NewMessage(update.Message.Chat.ID, "")
			if update.Message.IsCommand() {
				switch update.Message.Command() {
				case "start":
					msg.Text = "Hi, this inline bot can search Musescore for sheet music. See /help for more info"
				case "help":
					msg.Text = "This bot can search Musescore for sheet music. You can search by using me inline like this: @musescorebot Your search query"
				case "about":
					msg.Text = "This bot is free and open source software, licensed under the terms of the GNU AGPLv3. You can inspect the source code at https://gitlab.com/vimpostor/musescorebot"
				default:
					msg.Text = "I don't know that command. /help"
				}
			} else {
				msg.Text = "Beep, boop. I only understand commands. /help"
			}
			bot.Send(msg)
		} else if update.InlineQuery != nil {
			// search Musescore
			query := update.InlineQuery
			scores := getScores(query.Query, *msToken)
			resultsIf := make([]interface{}, len(scores))
			for i, s := range scores {
				resultsIf[i] = s
			}
			inlineConf := tgbotapi.InlineConfig{
				InlineQueryID: query.ID,
				Results:       resultsIf,
			}
			bot.AnswerInlineQuery(inlineConf)
		}
	}
}
